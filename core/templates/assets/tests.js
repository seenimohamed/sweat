'use strict';

define('sweat/tests/app.lint-test', [], function () {
  'use strict';

  QUnit.module('ESLint | app');

  QUnit.test('app.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'app.js should pass ESLint\n\n');
  });

  QUnit.test('components/header-panel.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/header-panel.js should pass ESLint\n\n');
  });

  QUnit.test('components/list-criteria.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/list-criteria.js should pass ESLint\n\n');
  });

  QUnit.test('components/side-nav.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/side-nav.js should pass ESLint\n\n');
  });

  QUnit.test('components/tool-tip.js', function (assert) {
    assert.expect(1);
    assert.ok(false, 'components/tool-tip.js should pass ESLint\n\n36:33 - \'Ember\' is not defined. (no-undef)');
  });

  QUnit.test('controllers/application.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'controllers/application.js should pass ESLint\n\n');
  });

  QUnit.test('controllers/customers.js', function (assert) {
    assert.expect(1);
    assert.ok(false, 'controllers/customers.js should pass ESLint\n\n1:10 - \'computed\' is defined but never used. (no-unused-vars)\n9:3 - Only string, number, symbol, boolean, null, undefined, and function are allowed as default properties (ember/avoid-leaking-state-in-ember-objects)\n15:3 - Only string, number, symbol, boolean, null, undefined, and function are allowed as default properties (ember/avoid-leaking-state-in-ember-objects)\n30:3 - Only string, number, symbol, boolean, null, undefined, and function are allowed as default properties (ember/avoid-leaking-state-in-ember-objects)');
  });

  QUnit.test('controllers/qp-list.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'controllers/qp-list.js should pass ESLint\n\n');
  });

  QUnit.test('resolver.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'resolver.js should pass ESLint\n\n');
  });

  QUnit.test('router.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'router.js should pass ESLint\n\n');
  });

  QUnit.test('routes/application.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/application.js should pass ESLint\n\n');
  });

  QUnit.test('routes/customers.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/customers.js should pass ESLint\n\n');
  });

  QUnit.test('routes/customers/new.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/customers/new.js should pass ESLint\n\n');
  });

  QUnit.test('routes/four-o-four.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/four-o-four.js should pass ESLint\n\n');
  });

  QUnit.test('routes/index.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/index.js should pass ESLint\n\n');
  });

  QUnit.test('routes/qp-list.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/qp-list.js should pass ESLint\n\n');
  });

  QUnit.test('routes/trainers.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/trainers.js should pass ESLint\n\n');
  });
});
define('sweat/tests/helpers/destroy-app', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = destroyApp;
  function destroyApp(application) {
    Ember.run(application, 'destroy');
  }
});
define('sweat/tests/helpers/module-for-acceptance', ['exports', 'qunit', 'sweat/tests/helpers/start-app', 'sweat/tests/helpers/destroy-app'], function (exports, _qunit, _startApp, _destroyApp) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  exports.default = function (name) {
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    (0, _qunit.module)(name, {
      beforeEach: function beforeEach() {
        this.application = (0, _startApp.default)();

        if (options.beforeEach) {
          return options.beforeEach.apply(this, arguments);
        }
      },
      afterEach: function afterEach() {
        var _this = this;

        var afterEach = options.afterEach && options.afterEach.apply(this, arguments);
        return Ember.RSVP.resolve(afterEach).then(function () {
          return (0, _destroyApp.default)(_this.application);
        });
      }
    });
  };
});
define('sweat/tests/helpers/start-app', ['exports', 'sweat/app', 'sweat/config/environment'], function (exports, _app, _environment) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = startApp;
  function startApp(attrs) {
    var attributes = Ember.merge({}, _environment.default.APP);
    attributes.autoboot = true;
    attributes = Ember.merge(attributes, attrs); // use defaults, but you can override;

    return Ember.run(function () {
      var application = _app.default.create(attributes);
      application.setupForTesting();
      application.injectTestHelpers();
      return application;
    });
  }
});
define('sweat/tests/integration/components/header-panel-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleForComponent)('header-panel', 'Integration | Component | header panel', {
    integration: true
  });

  (0, _emberQunit.test)('it renders', function (assert) {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });

    this.render(Ember.HTMLBars.template({
      "id": "j8Wj1P1O",
      "block": "{\"symbols\":[],\"statements\":[[1,[18,\"header-panel\"],false]],\"hasEval\":false}",
      "meta": {}
    }));

    assert.equal(this.$().text().trim(), '');

    // Template block usage:
    this.render(Ember.HTMLBars.template({
      "id": "+lmwFhBW",
      "block": "{\"symbols\":[],\"statements\":[[0,\"\\n\"],[4,\"header-panel\",null,null,{\"statements\":[[0,\"      template block text\\n\"]],\"parameters\":[]},null],[0,\"  \"]],\"hasEval\":false}",
      "meta": {}
    }));

    assert.equal(this.$().text().trim(), 'template block text');
  });
});
define('sweat/tests/integration/components/list-criteria-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleForComponent)('list-criteria', 'Integration | Component | list criteria', {
    integration: true
  });

  (0, _emberQunit.test)('it renders', function (assert) {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });

    this.render(Ember.HTMLBars.template({
      "id": "Xr/9xBU4",
      "block": "{\"symbols\":[],\"statements\":[[1,[18,\"list-criteria\"],false]],\"hasEval\":false}",
      "meta": {}
    }));

    assert.equal(this.$().text().trim(), '');

    // Template block usage:
    this.render(Ember.HTMLBars.template({
      "id": "W00jJfev",
      "block": "{\"symbols\":[],\"statements\":[[0,\"\\n\"],[4,\"list-criteria\",null,null,{\"statements\":[[0,\"      template block text\\n\"]],\"parameters\":[]},null],[0,\"  \"]],\"hasEval\":false}",
      "meta": {}
    }));

    assert.equal(this.$().text().trim(), 'template block text');
  });
});
define('sweat/tests/integration/components/side-nav-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleForComponent)('side-nav', 'Integration | Component | side nav', {
    integration: true
  });

  (0, _emberQunit.test)('it renders', function (assert) {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });

    this.render(Ember.HTMLBars.template({
      "id": "YKoUKq70",
      "block": "{\"symbols\":[],\"statements\":[[1,[18,\"side-nav\"],false]],\"hasEval\":false}",
      "meta": {}
    }));

    assert.equal(this.$().text().trim(), '');

    // Template block usage:
    this.render(Ember.HTMLBars.template({
      "id": "35cOzj0p",
      "block": "{\"symbols\":[],\"statements\":[[0,\"\\n\"],[4,\"side-nav\",null,null,{\"statements\":[[0,\"      template block text\\n\"]],\"parameters\":[]},null],[0,\"  \"]],\"hasEval\":false}",
      "meta": {}
    }));

    assert.equal(this.$().text().trim(), 'template block text');
  });
});
define('sweat/tests/test-helper', ['sweat/app', 'sweat/config/environment', '@ember/test-helpers', 'ember-qunit'], function (_app, _environment, _testHelpers, _emberQunit) {
  'use strict';

  (0, _testHelpers.setApplication)(_app.default.create(_environment.default.APP));

  (0, _emberQunit.start)();
});
define('sweat/tests/tests.lint-test', [], function () {
  'use strict';

  QUnit.module('ESLint | tests');

  QUnit.test('helpers/destroy-app.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'helpers/destroy-app.js should pass ESLint\n\n');
  });

  QUnit.test('helpers/module-for-acceptance.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'helpers/module-for-acceptance.js should pass ESLint\n\n');
  });

  QUnit.test('helpers/start-app.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'helpers/start-app.js should pass ESLint\n\n');
  });

  QUnit.test('integration/components/header-panel-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/components/header-panel-test.js should pass ESLint\n\n');
  });

  QUnit.test('integration/components/list-criteria-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/components/list-criteria-test.js should pass ESLint\n\n');
  });

  QUnit.test('integration/components/side-nav-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/components/side-nav-test.js should pass ESLint\n\n');
  });

  QUnit.test('test-helper.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'test-helper.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/application-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/application-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/customers-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/customers-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/customers/new-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/customers/new-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/four-o-four-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/four-o-four-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/index-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/index-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/qp-list-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/qp-list-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/trainers-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/trainers-test.js should pass ESLint\n\n');
  });
});
define('sweat/tests/unit/routes/application-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleFor)('route:application', 'Unit | Route | application', {
    // Specify the other units that are required for this test.
    // needs: ['controller:foo']
  });

  (0, _emberQunit.test)('it exists', function (assert) {
    var route = this.subject();
    assert.ok(route);
  });
});
define('sweat/tests/unit/routes/customers-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleFor)('route:customers', 'Unit | Route | customers', {
    // Specify the other units that are required for this test.
    // needs: ['controller:foo']
  });

  (0, _emberQunit.test)('it exists', function (assert) {
    var route = this.subject();
    assert.ok(route);
  });
});
define('sweat/tests/unit/routes/customers/new-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleFor)('route:customers/new', 'Unit | Route | customers/new', {
    // Specify the other units that are required for this test.
    // needs: ['controller:foo']
  });

  (0, _emberQunit.test)('it exists', function (assert) {
    var route = this.subject();
    assert.ok(route);
  });
});
define('sweat/tests/unit/routes/four-o-four-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleFor)('route:four-o-four', 'Unit | Route | four o four', {
    // Specify the other units that are required for this test.
    // needs: ['controller:foo']
  });

  (0, _emberQunit.test)('it exists', function (assert) {
    var route = this.subject();
    assert.ok(route);
  });
});
define('sweat/tests/unit/routes/index-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleFor)('route:index', 'Unit | Route | index', {
    // Specify the other units that are required for this test.
    // needs: ['controller:foo']
  });

  (0, _emberQunit.test)('it exists', function (assert) {
    var route = this.subject();
    assert.ok(route);
  });
});
define('sweat/tests/unit/routes/qp-list-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleFor)('route:qp-list', 'Unit | Route | qp list', {
    // Specify the other units that are required for this test.
    // needs: ['controller:foo']
  });

  (0, _emberQunit.test)('it exists', function (assert) {
    var route = this.subject();
    assert.ok(route);
  });
});
define('sweat/tests/unit/routes/trainers-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleFor)('route:trainers', 'Unit | Route | trainers', {
    // Specify the other units that are required for this test.
    // needs: ['controller:foo']
  });

  (0, _emberQunit.test)('it exists', function (assert) {
    var route = this.subject();
    assert.ok(route);
  });
});
require('sweat/tests/test-helper');
EmberENV.TESTS_FILE_LOADED = true;
//# sourceMappingURL=tests.map
