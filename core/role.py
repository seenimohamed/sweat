from flask import Flask, request, jsonify, render_template
from flask_restful import Resource, Api
from sqlalchemy import create_engine
from flask import json
from json import dumps

db_connect = create_engine('sqlite:///../sweat.db')
app = Flask(__name__)
api = Api(app)

class Roles(Resource) :

    def get(self) :
        conn = db_connect.connect() # connect to database

        if 'roleid' in request.args :
            querystring = "select * from roledetails where roleid = %d" %int(request.args['roleid'])
        else :
            querystring = "select * from roledetails"

        query = conn.execute(querystring) # This line performs query and returns json result
        queryresult = query.cursor.fetchall()
        if not queryresult :
            return jsonify({'result':'no record'})

        result = []
        for row in queryresult :
            rowdata = {'roleid':row[0],'rolename':row[1]}
            result.append(rowdata)

        return jsonify({'result' : result})


    def post(self) :
        conn = db_connect.connect()

        roleid = request.json['roleid']
        rolename = request.json['rolename']
        print(rolename)
        query = conn.execute("insert into roledetails(roleid, rolename) values({0},'{1}')".format(roleid,rolename))
        return jsonify({'status':'success'})

class Roles_Fetch(Resource) :

    def get(self) :
        conn = db_connect.connect() # connect to database
        roleid = request.args['roleid']
        print(roleid)

        query = conn.execute("select * from roledetails where roleid=%d" %int(roleid))
        # print(query.cursor.fetchall())
        result = query.cursor.fetchall()
        print(result)
        if not result :
            return jsonify({'result':'no record'})
        for row in result :
            print('{0} = {1}'.format(row[0],row[1]))
            print(row)
        return jsonify({'rolename': [i[1] for i in result]}) # Fetches second column that is role name

api.add_resource(Roles,'/roles')



@app.route('/')
def index():
    """ Displays the index page accessible at '/'
    """
    return render_template('index.html')

if __name__ == '__main__' :
    app.run(debug=True,host='localhost', port=4040)
